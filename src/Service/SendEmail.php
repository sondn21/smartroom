<?php
namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class SendEmail
{
    private $mailer;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SendEmail constructor.
     * @param MailerInterface $mailer
     * @param LoggerInterface $logger
     */
    public function __construct(MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }


    public function sendEmail(User $user)
    {

        try {
            $this->logger->debug('Start to send email to customer');
            $email = (new TemplatedEmail())
                ->from('admin@smartroom.dev.com')
                ->to(new Address($user->getEmail()))
                ->subject('Thanks for booking room!')

                // path of the Twig template to render
                ->htmlTemplate('emails/booking.html.twig')

                // pass variables (name => value) to the template
                ->context([
                    'expiration_date' => new \DateTime('+7 days'),
                    'username' => $user->getUsername(),
                ]);
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }
    }
}