<?php


namespace App\MessageHandler;

use App\Message\SendEmailBooking;
use App\Service\SendEmail;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SendEmailBookingHandler implements MessageHandlerInterface
{
    /**
     * @var SendEmail
     */
    private $mailer;

    public function __construct(SendEmail $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(SendEmailBooking $message)
    {
        // ... do some work - like sending an SMS message!
        $this->mailer->sendEmail($message->getContent());
    }
}