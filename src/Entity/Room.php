<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 */
class Room
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=4)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

//    /**
//     * @ORM\Column(type="date", nullable=true)
//     */
//    private $date;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $timeFrom;

      /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $timeTo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Booking", mappedBy="room", orphanRemoval=true)
     */
    private $bookings;

    public function __construct()
    {
        $this->availability_id = new ArrayCollection();
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTimeFrom()
    {
        return $this->timeFrom;
    }

    public function setTimeFrom($time_from)
    {
        $this->timeFrom = $time_from;

        return $this;
    }

    public function getTimeTo()
    {
        return $this->timeTo;
    }

    public function setTimeTo($time_to)
    {
        $this->timeTo = $time_to;

        return $this;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(Booking $booking): self
    {
        $this->booking = $booking;

        // set the owning side of the relation if necessary
        if ($booking->getRoom() !== $this) {
            $booking->setRoom($this);
        }

        return $this;
    }

    /**
     * @return Collection|Booking[]
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setRoom($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->contains($booking)) {
            $this->bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getRoom() === $this) {
                $booking->setRoom(null);
            }
        }

        return $this;
    }

//    public function getDate()
//    {
//        return $this->date;
//    }
//
//    public function setDate($date)
//    {
//        $this->date = $date;
//        return $this;
//    }
}
