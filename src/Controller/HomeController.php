<?php

namespace App\Controller;

use App\Entity\Room;
use App\Repository\BookingRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\RoomRepository;


class HomeController extends AbstractController
{

    /**
     * @Route("/home", name="home")
     * @param RoomRepository $roomRepository
     * @return Response
     */
    public function index(RoomRepository $roomRepository)
    {
        return $this->render('home/index.html.twig', [
            'rooms' => $roomRepository->findAll(),
        ]);
    }

    /**
     * @Route("/home/room_detail/{id}", name="room_detail", methods={"GET"})
     * @param Room $room
     * @return Response
     */
    public function roomDetail(Room $room)
    {
        return $this->render('room/show_detail.html.twig', [
            'room' => $room,
        ]);
    }
    /*
     * Render search bar on top
     */

    public function searchBarAction(Request $request)
    {
        $form = $this->createFormBuilder(null)
            ->add('keyword', TextType::class)
            ->getForm();
        return $this->render('search_bar.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/search", name="handleSearch")
     * @param Request $request
     * @param RoomRepository $roomRepository
     * @return Response
     */
    public function handleSearch(Request $request , RoomRepository $roomRepository)
    {
        $keyword = $request->request->get('form')['keyword'];
        $rooms = $roomRepository->listAvailableRoom($keyword);
        return $this->render('home/index.html.twig', [
            'rooms' => $rooms,
        ]);
    }

}
