<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ArticleController extends AbstractController
{
    public function createArticle(Request $request, ValidatorInterface $validator)
    {
        $article = new Article();

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // Validation
            $errors = $validator->validate($article);

            if (count($errors) > 0) {
                /*
                 * Uses a __toString method on the $errors variable which is a
                 * ConstraintViolationList object. This gives us a nice string
                 * for debugging.
                 */

                $errorsString = (string) $errors;
                return $this->render('article/validation.html.twig', [
                    'errors' => $errors,
                ]);
            }

            $article = $form->getData();


            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            $this->addFlash('success', 'Add article successfully');

            return $this->redirect('/view-article/' . $article->getId());

        }

        return $this->render(
            'article/edit.html.twig',
            array('form' => $form->createView())
        );

    }

    public function viewArticle($id)
    {
        $article = $this->getDoctrine()
            ->getRepository('App\Entity\Article')
            ->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }

        return $this->render(
            'article/view.html.twig',
            array('article' => $article)
        );
    }

    public function showArticles()
    {
        $articles = $this->getDoctrine()
            ->getRepository('App\Entity\Article')
            ->findAll();

        return $this->render(
            'article/show.html.twig',
            array('articles' => $articles)
        );
    }

    public function deleteArticle($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }

        $em->remove($article);
        $em->flush();

        return $this->redirect('/show-articles');
    }

    public function updateArticle(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('App\Entity\Article')->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'There are no articles with the following id: ' . $id
            );
        }

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $article = $form->getData();
            $em->flush();
            return $this->redirect('/view-article/' . $id);
        }

        return $this->render(
            'article/edit.html.twig',
            array('form' => $form->createView())
        );
    }
}
