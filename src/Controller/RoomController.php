<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Room;
use App\Entity\Availability;
use App\Form\RoomType;
use App\Message\SendEmailBooking;
use App\Repository\BookingRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Service\SendEmail;

/**
 * @Route("/room")
 */
class RoomController extends AbstractController
{

    private $bookingRepository;

    /**
     * RoomController constructor.
     * @param $bookingRepository
     */
    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }


    /**
     * @Route("/", name="room_index", methods={"GET"})
     * @param RoomRepository $roomRepository
     * @return Response
     */
    public function index(RoomRepository $roomRepository): Response
    {
        return $this->render('room/index.html.twig', [
            'rooms' => $roomRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="room_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            $param = $request->request->all();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            return $this->redirectToRoute('room_index');
        }

        return $this->render('room/new.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="room_show", methods={"GET"})
     * @param Room $room
     * @return Response
     */
    public function show(Room $room): Response
    {
        return $this->render('room/show.html.twig', [
            'room' => $room,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="room_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Room $room
     * @return Response
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('room_index');
        }

        return $this->render('room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="room_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();
        }

        return $this->redirectToRoute('room_index');
    }

    /**
     * @Route("/booking/{id}", name="booking_room", methods={"GET", "POST"})
     * @param Request $request
     * @param Room $room
     * @param Security $security
     * @param SendEmail $sendEmail
     * @param MessageBus $messageBus
     * @return Response
     */
    public function booking(Request $request, Room $room, Security $security, SendEmail $sendEmail, MessageBusInterface $messageBus): Response
    {
        // Check user has already booking this room or not
        $user = $security->getUser();
        $booking = $this->bookingRepository->checkBooking($room);
        if ($booking) {
            $this->addFlash('Error', 'This room has already booked by someone. Please select another room!');
            return $this->redirectToRoute('home');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $booking = new Booking();
        $booking->setRoom($room);
        $booking->setUser($user);
        $booking->setCheckIn($room->getTimeFrom());
        $booking->setCheckOut($room->getTimeTo());
        $entityManager->persist($booking);
        $entityManager->flush();

        // Send email
        $message = new SendEmailBooking($user);
        $messageBus->dispatch($message);


//        $sendEmail->sendEmail($user);
        $this->addFlash('Success', 'Booked successfully!');
        return $this->redirectToRoute('home');
    }


}
