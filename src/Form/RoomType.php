<?php

namespace App\Form;

use App\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('description')
            ->add('timeFrom', DateType::class, array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',

                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // adds a class that can be selected in JavaScript
                'attr' => ['class' => 'js-datepicker-time-from'],
            ))
            ->add('timeTo', DateType::class, array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',

                // prevents rendering it as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // adds a class that can be selected in JavaScript
                'attr' => ['class' => 'js-datepicker-time-to'],

            ))
//            ->add('date', DateType::class, array(
//                'required' => false,
//                'widget' => 'single_text',
//                'format' => 'yyyy-MM-dd',
//
//                // prevents rendering it as type="date", to avoid HTML5 date pickers
//                'html5' => false,
//
//                // adds a class that can be selected in JavaScript
//                'attr' => ['class' => 'js-datepicker-time-to']
//            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
        ]);
    }
}
