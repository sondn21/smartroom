<?php

namespace App\Repository;

use App\Entity\Room;
use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{

    private $bookingRepository;

    public function __construct(ManagerRegistry $registry, BookingRepository $bookingRepository)
    {
        parent::__construct($registry, Room::class);
        $this->bookingRepository = $bookingRepository;
    }

    public function listAvailableRoom(string $keyword)
    {
        $bookings = $this->bookingRepository->createQueryBuilder('b');
//        $bookings->select('r')
//            ->join(Room::class, 'r', 'WITH', 'r.id = b.room')
//        ;
        $bookings_arr = $bookings->getQuery()->getResult();
        $res = [];
        foreach ($bookings_arr as $item) {
            array_push($res, $item->getRoom()->getId());
        }
        $qb = $this->createQueryBuilder('room');
        $qb->where(
            $qb->expr()->andX(
              $qb->expr()->orX(
                  $qb->expr()->like('room.name', ':keyword'),
                  $qb->expr()->like('room.description', ':keyword')
              )
            ),
            $qb->expr()->notIn('room.id', $res)
        )->setParameter('keyword', '%'.$keyword.'%')
            ->orderBy('room.id', 'ASC');

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Room[] Returns an array of Room objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Room
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
