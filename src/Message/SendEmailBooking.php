<?php


namespace App\Message;


use App\Entity\User;

class SendEmailBooking
{
    private $content;

    /**
     * SendEmailBooking constructor.
     * @param $content
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    public function getContent(): User
    {
        return $this->content;
    }

}